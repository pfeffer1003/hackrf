#!/usr/bin/env python


import os
import sys
import time
import subprocess



class Client(object):

    def __init__(self,user,host,alias):

        self.__user = user
        self.__host = host
        self.__alias = alias

        self.__ssh_access = "ssh %s@%s "%(self.__user,self.__host)
        self.__cmd = "/home/%s/.hackrf/bin/hackrf-tx-client"%self.__user 


    def log(self): os.system(self.__ssh_access+"'%s %s log'"%(self.__cmd,self.__alias))
    def info(self): os.system(self.__ssh_access+"'%s %s info'"%(self.__cmd,self.__alias))
    def stop(self): os.system(self.__ssh_access+"'%s %s stop'"%(self.__cmd,self.__alias))
    def reset(self): os.system(self.__ssh_access+"'%s %s reset'"%(self.__cmd,self.__alias))
    def start(self): os.system(self.__ssh_access+"'%s %s start'"%(self.__cmd,self.__alias))
    def restart(self): os.system(self.__ssh_access+"'%s %s restart'"%(self.__cmd,self.__alias))


    def daemon_show(self): os.system(self.__ssh_access+"'%s %s daemon show'"%(self.__cmd,self.__alias))
    def daemon_stop(self): os.system(self.__ssh_access+"'%s %s daemon stop'"%(self.__cmd,self.__alias))
    def daemon_start(self): os.system(self.__ssh_access+"'%s %s daemon start'"%(self.__cmd,self.__alias))
    def daemon_status(self): os.system(self.__ssh_access+"'%s %s daemon status'"%(self.__cmd,self.__alias))
    def daemon_enable(self): os.system(self.__ssh_access+"'%s %s daemon enable'"%(self.__cmd,self.__alias))
    def daemon_disable(self): os.system(self.__ssh_access+"'%s %s daemon disable'"%(self.__cmd,self.__alias))
    def daemon_restart(self): os.system(self.__ssh_access+"'%s %s daemon restart'"%(self.__cmd,self.__alias))
    def daemon_journal(self): os.system(self.__ssh_access+"'%s %s daemon journal'"%(self.__cmd,self.__alias))


    def get_carrier(self):

        try:
            carrier = subprocess.check_output(self.__ssh_access+"'%s %s get carrier'"%(self.__cmd,self.__alias),shell=True)
            carrier = carrier.replace('\n','')
            try:
                carrier = float(carrier)
            except ValueError:
                carrier = None # means server not running
        except subprocess.CalledProcessError:
            carrier = 'no-response'
        return carrier

    def get_if_gain(self):

        try:
            if_gain = subprocess.check_output(self.__ssh_access+"'%s %s get if-gain'"%(self.__cmd,self.__alias),shell=True)
            if_gain = if_gain.replace('\n','')
            try:
                if_gain = float(if_gain)
            except ValueError:
                if_gain = None # means server not running
        except subprocess.CalledProcessError:
            if_gain = 'no-response'
        return if_gain

    def get_amplitude(self):

        try:
            amplitude = subprocess.check_output(self.__ssh_access+"'%s %s get amplitude'"%(self.__cmd,self.__alias),shell=True)
            amplitude = amplitude.replace('\n','')
            try:
                amplitude = float(amplitude)
            except ValueError:
                amplitude = None # means server not running
        except subprocess.CalledProcessError:
            amplitude = 'no-response'
        return amplitude

    def set_carrier(self,carrier): os.system(self.__ssh_access+"'%s %s set carrier %s'"%(self.__cmd,self.__alias,float(carrier)))
    def set_if_gain(self,if_gain): os.system(self.__ssh_access+"'%s %s set if_gain %s'"%(self.__cmd,self.__alias,float(if_gain)))
    def set_amplitude(self,amplitude): os.system(self.__ssh_access+"'%s %s set amplitude %s'"%(self.__cmd,self.__alias,float(amplitude)))



if __name__ == "__main__":

    cli = Client(user="radar",host="localhost",alias="default")
    cli.daemon_show()
    sys.exit()